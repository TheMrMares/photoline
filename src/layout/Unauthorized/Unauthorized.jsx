import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'

import SignIn from '../../pages/SignIn'

import styles from './Unauthorized.style'

export class Unauthorized extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  }

  render() {
    const { classes } = this.props
    return (
      <Switch>
        <Route component={SignIn} to="/sign-in" />
        <Redirect from="*" to="/sign-in" />
      </Switch>
    )
  }
}

export default withStyles(styles)(Unauthorized)
