import grey from '@material-ui/core/colors/grey'
import lightBlue from '@material-ui/core/colors/lightBlue'

export default {
  overrides: {
    MuiButton: {
      root: {
        fontFamily: 'inherit',
        fontWeight: 'bold',
        letterSpacing: 1,
        padding: '8px 48px',
        textTransform: 'uppercase',
      },
    },
    MuiPaper: {
      root: {
        boxShadow: 'none !important',
      },
      rounded: {
        borderRadius: 0,
      },
    },
  },
  palette: {
    primary: {
      contrastText: '#ffffff',
      main: grey[200],
    },
    secondary: {
      contrastText: '#ffffff',
      main: lightBlue[500],
    },
  },
  typography: {
    body2: {
      fontWeight: 'bold',
    },
    button: {
      textTransform: 'none',
    },
    fontFamily: 'Roboto, sans-serif',
    subheading: {
      color: 'inherit',
    },
    subtitle1: {
      color: 'inherit',
      fontSize: 'inherit',
      fontWeight: 'inherit',
    },
    useNextVariants: true,
  },
}
