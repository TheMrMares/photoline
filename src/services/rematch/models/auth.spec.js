import Immutable from 'immutable'
import auth, { initialState } from './auth'
import firebase from '../../firebase'

let initialStateMock

describe('auth model: ', () => {
  describe('effects: ', () => {
    describe('signInAsync', () => {
      beforeEach(() => {
        firebase.auth = jest.fn().mockReturnValue({
          signInWithEmailAndPassword: jest.fn().mockReturnValue({
            user: 'user123',
          }),
        })
      })
      it('it signs user in', async () => {
        await auth.effects.signInAsync(
          {
            email: 'example@email',
            password: 'example-password',
          },
          undefined
        )
        expect(firebase.auth().signInWithEmailAndPassword).toBeCalled()
        expect(firebase.auth().signInWithEmailAndPassword).toHaveBeenCalledWith(
          'example@email',
          'example-password'
        )
      })
    })
    describe('signUpAsync', () => {
      beforeEach(() => {
        firebase.auth = jest.fn().mockReturnValue({
          createUserWithEmailAndPassword: jest.fn().mockReturnValue({
            user: 'user123',
          }),
        })
      })
      it('it signs user up', async () => {
        await auth.effects.signUpAsync(
          {
            email: 'example@email',
            password: 'example-password',
          },
          undefined
        )
        expect(firebase.auth().createUserWithEmailAndPassword).toBeCalled()
        expect(firebase.auth().createUserWithEmailAndPassword).toHaveBeenCalledWith(
          'example@email',
          'example-password'
        )
      })
    })
  })
  describe('reducers: ', () => {
    beforeEach(() => {
      initialStateMock = { ...initialState }
    })
    it('clearAuthState', () => {
      const res = auth.reducers.clearAuthState()
      expect(res).toEqual({
        ...initialStateMock,
      })
    })
    it('updateAuthState', () => {
      const res = auth.reducers.updateAuthState(initialStateMock, {
        authorizedUser: { name: 'testName' },
        isAuthorized: true,
      })
      expect(res).toEqual({
        ...initialStateMock,
        authorizedUser: Immutable.fromJS({
          name: 'testName',
        }),
        isAuthorized: true,
      })
    })
  })
})
