import Immutable from 'immutable'
import { toast } from 'react-toastify'
import firebase from '../../firebase'

export const initialState = {
  authorizedUser: Immutable.Map({}),
  isAuthed: false,
}

const auth = {
  effects: {
    async signInAsync(payload) {
      const { email, password } = payload
      try {
        await firebase.auth().signInWithEmailAndPassword(email, password)
        toast.success('Successfully logged in!')
      } catch (err) {
        toast.error(err.message)
      }
    },
    async signUpAsync(payload) {
      const { email, password } = payload
      try {
        await firebase.auth().createUserWithEmailAndPassword(email, password)
        toast.success('Account has been created!')
      } catch (err) {
        toast.error(err.message)
      }
    },
  },
  reducers: {
    clearAuthState() {
      return {
        ...initialState,
      }
    },
    updateAuthState(state, payload) {
      const { authorizedUser, isAuthorized } = payload
      return {
        ...state,
        authorizedUser: Immutable.fromJS({ ...authorizedUser }),
        isAuthorized,
      }
    },
  },
  state: initialState,
}

export default auth
