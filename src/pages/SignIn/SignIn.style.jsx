export default ({ palette, spacing }) => ({
  appBar: {
    background: palette.background.paper,
    boxShadow: 'none',
    width: 'auto',
  },
  paper: {
    marginTop: spacing.unit * 10,
    padding: spacing.unit * 8,
  },
  root: {
    alignItems: 'center',
    background: palette.primary.main,
    display: 'flex',
    flex: '1 1 auto',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding: spacing.unit * 3,
  },
  tab: {
    color: palette.text.primary,
  },
  tabTitle: {
    fontSize: '2.3rem',
    textAlign: 'center',
    width: 'auto',
  }
})
