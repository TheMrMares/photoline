import React from 'react'
import { shallow } from 'enzyme'
import { mocks } from '../../../utils/tests'

import { LoginForm } from './LoginForm'

const initComponent = overrides => {
  const mockProps = {
    classes: mocks.classesProxy,
  }
  const mockMethods = {
    signInAsync: jest.fn(),
  }
  const wrapper = shallow(<LoginForm {...mockProps} {...mockMethods} {...overrides} />)
  return { mockProps, wrapper }
}

describe('LoginForm: ', () => {
  it('renders without crashing', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toBeTruthy()
  })
  it('should render as expected', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toMatchSnapshot()
  })
})
