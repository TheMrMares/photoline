import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import EmailIcon from '@material-ui/icons/Email'
import SecurityIcon from '@material-ui/icons/Security'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'

import { initialValues, validationSchema } from './LoginFormHelper'
import FormField from '../../../components/FormField'

import styles from './LoginForm.style'

export class LoginForm extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    signInAsync: PropTypes.func,
  }

  constructor() {
    super()

    this.submit = this.submit.bind(this)
  }

  submit(values) {
    const { signInAsync } = this.props
    const { email, password } = values
    signInAsync({
      email,
      password,
    })
  }

  render() {
    const { classes } = this.props
    return (
      <Formik
        initialValues={initialValues}
        onSubmit={this.submit}
        validateOnBlur
        validationSchema={validationSchema}
      >
        {formikProps => {
          const { dirty, isValid, handleSubmit } = formikProps
          return (
            <div className={classes.root}>
              <div className={classes.row}>
                <FormField
                  InputProps={{
                    startAdornment:
                    <InputAdornment position="start">
                      <EmailIcon className={classes.icon} />
                    </InputAdornment>,
                  }}
                  className={classes.field}
                  label="Email"
                  name="email"
                  onSubmit={this.submit}
                  placeholder="Account email"
                  type="email"
                />
              </div>
              <div className={classes.row}>
                <FormField
                  InputProps={{
                    startAdornment:
                    <InputAdornment position="start">
                      <SecurityIcon className={classes.icon} />
                    </InputAdornment>,
                  }}
                  className={classes.field}
                  label="Password"
                  name="password"
                  placeholder="Password"
                  type="password"
                />
              </div>
              <div className={classes.row}>
                <Button
                  className={classes.submitButton}
                  color="secondary"
                  disabled={!dirty || !isValid}
                  onClick={handleSubmit}
                  type="submit"
                  variant="contained"
                >
                Proceed
                </Button>
              </div>
            </div>
          )
        }}
      </Formik>
    )
  }
}

const mapDispatch = ({
  auth: {
    signInAsync,
  },
}) => ({
  signInAsync,
})

export default connect(null, mapDispatch)(withStyles(styles)(LoginForm))
