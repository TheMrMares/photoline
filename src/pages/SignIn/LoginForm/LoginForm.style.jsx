export default ({ spacing }) => ({
  field: {
    marginBottom: spacing.unit * 1,
    marginTop: spacing.unit * 3,
  },
  icon: {
    fontSize: '1rem',
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: spacing.unit * 2,
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
  },
  submitButton: {
    marginTop: spacing.unit * 4,
  },
})
