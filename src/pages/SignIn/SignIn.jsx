import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'

import RegisterForm from './RegisterForm'
import LoginForm from './LoginForm'

import styles from './SignIn.style'

export class SignIn extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
  }

  constructor() {
    super()

    this.state = {
      tab: 0,
    }

    this.handleTabChange = this.handleTabChange.bind(this)
  }

  handleTabChange(evt, value) {
    this.setState({
      tab: value,
    })
  }

  render() {
    const { classes } = this.props
    const { tab } = this.state
    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar} position="static">
          <Tabs onChange={this.handleTabChange} value={tab}>
            <Tab className={classes.tab} label="Login" />
            <Tab className={classes.tab} label="Register" />
          </Tabs>
        </AppBar>
        <Paper className={classes.paper}>
          {tab === 0 && (
            <React.Fragment>
              <Typography className={classes.tabTitle} variant="h1">
                Login
              </Typography>
              <LoginForm />
            </React.Fragment>
          )}
          {tab === 1 && (
            <React.Fragment>
              <Typography className={classes.tabTitle} variant="h1">
                Register
              </Typography>
              <RegisterForm />
            </React.Fragment>
          )}
        </Paper>
      </div>
    )
  }
}

export default withStyles(styles)(SignIn)
