import React from 'react'
import { shallow } from 'enzyme'
import { mocks } from '../../utils/tests'

import { SignIn } from './SignIn'

jest.mock('./LoginForm', () => 'LoginForm')
jest.mock('./RegisterForm', () => 'RegisterForm')

const initComponent = overrides => {
  const mockProps = {
    classes: mocks.classesProxy,
  }
  const mockMethods = {}
  const wrapper = shallow(<SignIn {...mockProps} {...mockMethods} {...overrides} />)
  return { mockProps, wrapper }
}

describe('SignIn: ', () => {
  it('renders without crashing', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toBeTruthy()
  })
  it('should render as expected', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toMatchSnapshot()
  })
})
