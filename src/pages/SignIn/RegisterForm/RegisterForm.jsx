import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import EmailIcon from '@material-ui/icons/Email'
import SecurityIcon from '@material-ui/icons/Security'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'

import { initialValues, validationSchema } from './RegisterFormHelper'
import FormField from '../../../components/FormField'

import styles from './RegisterForm.style'

export class RegisterForm extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    signUpAsync: PropTypes.func,
  }

  constructor() {
    super()

    this.submit = this.submit.bind(this)
  }

  submit(values) {
    const { signUpAsync } = this.props
    const { email, password } = values
    signUpAsync({
      email, password,
    })
  }

  render() {
    const { classes } = this.props
    return (
      <Formik
        enableReinitialize
        initialValues={initialValues}
        onSubmit={this.submit}
        validateOnBlur
        validationSchema={validationSchema}
      >
        {formikProps => {
          const { dirty, isValid, handleSubmit } = formikProps
          return (
            <div className={classes.root}>
              <div className={classes.row}>
                <div className={classes.col}>
                  <div className={classes.row}>
                    <FormField
                      InputProps={{
                        startAdornment:
                        <InputAdornment position="start">
                          <EmailIcon className={classes.icon} />
                        </InputAdornment>,
                      }}
                      className={classes.field}
                      label="Email"
                      name="email"
                      onSubmit={this.submit}
                      placeholder="Account email"
                      type="email"
                    />
                  </div>
                  <div className={classes.row}>
                    <FormField
                      InputProps={{
                        startAdornment:
                        <InputAdornment position="start">
                          <EmailIcon className={classes.icon} />
                        </InputAdornment>,
                      }}
                      className={classes.field}
                      label="Email confirmation"
                      name="confirmEmail"
                      onSubmit={this.submit}
                      placeholder="Confirm account email"
                      type="email"
                    />
                  </div>
                </div>
                <div className={classes.col}>
                  <div className={classes.row}>
                    <FormField
                      InputProps={{
                        startAdornment:
                        <InputAdornment position="start">
                          <SecurityIcon className={classes.icon} />
                        </InputAdornment>,
                      }}
                      className={classes.field}
                      label="Password"
                      name="password"
                      placeholder="Password"
                      type="password"
                    />
                  </div>
                  <div className={classes.row}>
                    <FormField
                      InputProps={{
                        startAdornment:
                        <InputAdornment position="start">
                          <SecurityIcon className={classes.icon} />
                        </InputAdornment>,
                      }}
                      className={classes.field}
                      label="Password confirmation"
                      name="confirmPassword"
                      placeholder="Confirm password"
                      type="password"
                    />
                  </div>
                </div>
              </div>
              <div className={classes.row}>
                <Button
                  className={classes.submitButton}
                  color="secondary"
                  disabled={!dirty || !isValid}
                  onClick={handleSubmit}
                  type="submit"
                  variant="contained"
                >
                Proceed
                </Button>
              </div>
            </div>
          )
        }}
      </Formik>
    )
  }
}

const mapDispatch = ({
  auth: {
    signUpAsync,
  },
}) => ({
  signUpAsync,
})

export default connect(null, mapDispatch)(withStyles(styles)(RegisterForm))
