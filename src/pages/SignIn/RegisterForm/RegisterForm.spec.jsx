import React from 'react'
import { shallow } from 'enzyme'
import { mocks } from '../../../utils/tests'

import { RegisterForm } from './RegisterForm'

const initComponent = overrides => {
  const mockProps = {
    classes: mocks.classesProxy,
  }
  const mockMethods = {
    signUpAsync: jest.fn(),
  }
  const wrapper = shallow(<RegisterForm {...mockProps} {...mockMethods} {...overrides} />)
  return { mockProps, wrapper }
}

describe('RegisterForm: ', () => {
  it('renders without crashing', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toBeTruthy()
  })
  it('should render as expected', () => {
    const { wrapper } = initComponent()
    expect(wrapper).toMatchSnapshot()
  })
})
