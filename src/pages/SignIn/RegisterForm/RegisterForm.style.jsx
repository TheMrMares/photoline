export default ({ spacing }) => ({
  col: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    margin: `0 ${spacing.unit * 2}px`,
  },
  field: {
    marginBottom: spacing.unit * 1,
    marginTop: spacing.unit * 3,
  },
  icon: {
    fontSize: '1rem',
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: spacing.unit * 2,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  submitButton: {
    marginTop: spacing.unit * 4,
  },
})
