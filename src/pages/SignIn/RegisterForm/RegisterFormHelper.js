import * as Yup from 'yup'

export const initialValues = {
  confirmEmail: '',
  confirmPassword: '',
  email: '',
  password: '',
}

export const validationSchema = Yup.object().shape({
  confirmEmail: Yup.string()
    .oneOf([Yup.ref('email'), null], 'Email adresses have to be same')
    .required('Emailconfirmation is required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords have to be same')
    .required('Password confirmationion is required'),
  email: Yup.string()
    .email('Email is invalid')
    .required('Email adress is required'),
  password: Yup.string().min(6, 'Password must be min. 6 characters long'),
})
